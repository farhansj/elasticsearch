# Clone the repository
```bash
$ git clone https://gitlab.com/farhansj/elasticsearch.git
```

# Create .env file
```bash
$ vi .env
```
Content:
```
CERTS_DIR=/usr/share/elasticsearch/config/certificates
ELASTIC_PASSWORD="your elasticsearch password"
```
update the default elasticsearch password

# Create certs
```bash
$ docker-compose -f create-certs.yml run --rm create_certs
```

# Create containers
```bash
$ docker-compose up -d
```

# Test query
```bash
$ docker run --rm -v elasticsearch_certs:/certs --network=elasticsearch_elastic docker.elastic.co/elasticsearch/elasticsearch:7.6.1 curl --cacert /certs/ca/ca.crt -u elastic:<your elasticsearch password>  https://es01:9200/_cat/indices
```
